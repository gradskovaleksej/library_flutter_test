import 'dart:convert';

BookInOrder bookInOrderFromJson(String str) => BookInOrder.fromJson(json.decode(str));

String bookInOrderToJson(BookInOrder data) => json.encode(data.toJson());

class BookInOrder {
  BookInOrder({
    this.idOrder,
    this.idBook,
  });

  int idOrder;
  int idBook;

  factory BookInOrder.fromJson(Map<String, dynamic> json) => BookInOrder(
    idOrder: json["id_order"],
    idBook: json["id_book"],
  );

  Map<String, dynamic> toJson() => {
    "id_order": idOrder,
    "id_book": idBook,
  };
}
