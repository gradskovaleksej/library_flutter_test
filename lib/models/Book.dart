import 'dart:convert';

Book bookFromJson(String str) => Book.fromJson(json.decode(str));

String bookToJson(Book data) => json.encode(data.toJson());

class Book {
  Book({
    this.id,
    this.idOfGenre,
    this.isbn,
    this.name,
    this.author,
    this.genre,
    this.year,
    this.quantityOfPages,
    this.publisher,
    this.linkImg,
    this.price,
    this.quantityInStock,
  });

  int id;
  int idOfGenre;
  String isbn;
  String name;
  String author;
  String genre;
  int year;
  int quantityOfPages;
  String publisher;
  String linkImg;
  int price;
  int quantityInStock;

  factory Book.fromJson(Map<String, dynamic> json) => Book(
    id: json["id"] == null ? null : json["id"],
    idOfGenre: json["id_of_genre"] == null ? null : json["id_of_genre"],
    isbn: json["ISBN"] == null ? null : json["ISBN"],
    name: json["name"] == null ? null : json["name"],
    author: json["author"] == null ? null : json["author"],
    year: json["year"] == null ? null : json["year"],
    quantityOfPages: json["quantityOfPages"] == null ? null : json["quantityOfPages"],
    publisher: json["publisher"] == null ? null : json["publisher"],
    linkImg: json["linkImg"] == null ? null : json["linkImg"],
    price: json["price"] == null ? null : json["price"],
    quantityInStock: json["quantityInStock"] == null ? null : json["quantityInStock"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "id_of_genre": idOfGenre == null ? null : idOfGenre,
    "ISBN": isbn == null ? null : isbn,
    "name": name == null ? null : name,
    "author": author == null ? null : author,
    "year": year == null ? null : year,
    "quantity_of_pages": quantityOfPages == null ? null : quantityOfPages,
    "publisher": publisher == null ? null : publisher,
    "linkImg": linkImg == null ? null : linkImg,
    "price": price == null ? null : price,
    "quantity_in_stock": quantityInStock == null ? null : quantityInStock
    ,
  };
}
