import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  User({
    this.idUser,
    this.fullname,
    this.phone,
    this.address,
    this.birthday,
    this.gender,
  });

  int idUser;
  String fullname;
  String phone;
  String address;
  String birthday;
  String gender;

  factory User.fromJson(Map<String, dynamic> json) => User(
    idUser: json["id"],
    fullname: json["fullname"],
    phone: json["phone"],
    address: json["address"],
    birthday: json["birthday"],
    gender: json["gender"],
  );

  Map<String, dynamic> toJson() => {
    "id": idUser,
    "fullname": fullname,
    "phone": phone,
    "address": address,
    "birthday": birthday,
    "gender": gender,
  };
}
