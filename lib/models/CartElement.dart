import 'dart:convert';

import 'Book.dart';

CartElement cartElementFromJson(String str) =>
    CartElement.fromJson(json.decode(str));

String cartElementToJson(CartElement data) => json.encode(data.toJson());

class CartElement {
  CartElement({
    this.id,
    this.quantity,
    this.book,
  });

  int id;
  int quantity;
  Book book;

  get totalPrice {
    return book.price * quantity;
  }

  void quantityAdd() {
    quantity++;
  }

  void quantityDecrease() {
    quantity--;
  }

  factory CartElement.fromJson(Map<String, dynamic> json) => CartElement(
        id: json["id"],
        quantity: json["quantity"],
        book: json["Book"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "quantity": quantity,
        "Book": book,
      };
}
