

import 'package:flutter/cupertino.dart';
import 'package:library_flutter/models/CartElement.dart';


class Cart extends ChangeNotifier {
  final List<CartElement> _elements = [];

  get items {
    return _elements;
  }

  get total {
    var total = 0;
    for (CartElement cartElement in items) {
      total += (cartElement.book.price * cartElement.quantity);
    }
    return total;
  }

  get length => _elements.length;

  void addToCart(CartElement newElement) {
    _elements.add(newElement);
    notifyListeners();
  }

  void removeFromCart(CartElement cartElement) {
    _elements.remove(cartElement);
    notifyListeners();
  }

  void clearCart(){
    _elements.clear();
    notifyListeners();
  }
}
