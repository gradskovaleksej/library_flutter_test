import 'dart:convert';

Order orderFromJson(String str) => Order.fromJson(json.decode(str));

String orderToJson(Order data) => json.encode(data.toJson());

class Order {
  Order({
    this.id,
    this.idUser,
    this.date,
    this.status,
  });

  int id;
  int idUser;
  String date;
  String status;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
    id: json["id"],
    idUser: json["id_user"],
    date: json["date"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "id_user": idUser,
    "date": date,
    "status": status,
  };
}
