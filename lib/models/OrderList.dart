import 'package:flutter/cupertino.dart';
import 'package:library_flutter/data/Database.dart';
import 'package:library_flutter/models/Order.dart';

class OrderList extends ChangeNotifier{
  List<Order> _orderList = [];

  get items{
    return _orderList;
  }

  get quantity{
    return _orderList.length;
  }

  void addToOrders(Order newOrder){
    _orderList.add(newOrder);
    notifyListeners();
  }

  void removeFromOrders(Order order){
    _orderList.remove(order);
    notifyListeners();
  }

  void setOrderStatus(Order order, newStatus){
    order.status = newStatus;
    notifyListeners();
  }
}