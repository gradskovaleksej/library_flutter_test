import 'dart:io';

import 'package:library_flutter/models/BookInOrder.dart';
import 'package:library_flutter/models/Order.dart';
import 'package:library_flutter/models/User.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:library_flutter/models/Book.dart';
import 'package:path/path.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TestDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE books ("
          "id	INTEGER UNIQUE,"
          "id_of_genre	INTEGER,"
          "ISBN	TEXT,"
          "name	TEXT,"
          "author	TEXT,"
          "year	INTEGER,"
          "quantity_of_pages	INTEGER,"
          "publisher	TEXT,"
          "linkImg	TEXT,"
          "price	INTEGER,"
          "quantity_in_stock	INTEGER,"
          "number_of_views	INTEGER,"
          "PRIMARY KEY(id AUTOINCREMENT))");
      await db.execute("CREATE TABLE genres("
          "id_of_genre	INTEGER UNIQUE,"
          "name	TEXT,"
          "quantity_of_books	INTEGER,"
          "number_of_views	INTEGER,"
          "PRIMARY KEY(id_of_genre AUTOINCREMENT))");
      await db.execute("CREATE TABLE users("
          "id INTEGER UNIQUE,"
          "fullname TEXT,"
          "phone TEXT,"
          "address TEXT,"
          "birthday TEXT,"
          "gender TEXT,"
          "PRIMARY KEY(id AUTOINCREMENT))");
      await db.execute("CREATE TABLE orders("
          "id INTEGER UNIQUE,"
          "id_user INTEGER,"
          "date TEXT,"
          "status TEXT,"
          "PRIMARY KEY(id AUTOINCREMENT))");
      await db.execute("CREATE TABLE book_in_order("
          "id_order INTEGER,"
          "id_book INTEGER)");
    });
  }

  newBookInOrder(BookInOrder bookInOrder) async {
    final db = await database;
    var res = await db.insert("book_in_order", bookInOrder.toJson());
    return res;
  }

  newOrder(Order newOrder) async {
    final db = await database;
    var res = await db.insert("orders", newOrder.toJson());
    return res;
  }

  newUser(User newUser) async {
    final db = await database;
    var res = await db.insert("users", newUser.toJson());
    return res;
  }

  getBooksInOrder(int idOrder) async {
    final db = await database;
    var res = await db.query("book_in_order", where: "id_order = ?", whereArgs: [idOrder]);
    return res.isNotEmpty ? Book.fromJson(res.first) : Null;
  }

   Future<User> getUser(int id) async {
    final db = await database;
    var res = await db.query("users", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? User.fromJson(res.first) : Null;
  }

  Future<List<User>> getAllUsers() async{
    final db = await database;
    var res = await db.query("users");
    List<User> list =
    res.isNotEmpty ? res.map((c) => User.fromJson(c)).toList() : [];
    return list;
  }

  newBook(Book newBook) async {
    final db = await database;
    var res = await db.insert("books", newBook.toJson());
    return res;
  }

  getBook(int id) async {
    final db = await database;
    var res = await db.query("books", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Book.fromJson(res.first) : Null;
  }

  Future<List<Book>> getAllBooks() async {
    final db = await database;
    var res = await db.query("books");
    List<Book> list =
        res.isNotEmpty ? res.map((c) => Book.fromJson(c)).toList() : [];
    return list;
  }

  Future<List<Book>> getBookForGenre(int idOfGenre) async {
    final db = await database;
    var res = await db
        .query("books", where: "id_of_genre = ?", whereArgs: [idOfGenre]);
    List<Book> list =
        res.isNotEmpty ? res.map((c) => Book.fromJson(c)).toList() : [];
    return list;
  }

  deleteBook(int id) async {
    final db = await database;
    db.delete("books", where: "id = ?", whereArgs: [id]);
  }
  Future<List<Order>> getOrders() async{
    final db = await database;
    var res = await db.query("orders");
    List<Order> list =
    res.isNotEmpty ? res.map((c) => Order.fromJson(c)).toList() : [];
    return list;
  }

  getOrder(int id) async{
    final db = await database;
    var res = await db.query("orders", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Book.fromJson(res.first) : Null;
  }
}
