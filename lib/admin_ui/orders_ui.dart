import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:library_flutter/data/Database.dart';
import 'package:library_flutter/models/Cart.dart';
import 'package:library_flutter/models/CartElement.dart';
import 'package:library_flutter/models/Order.dart';
import 'package:library_flutter/models/OrderList.dart';
import 'package:library_flutter/models/User.dart';
import 'package:provider/provider.dart';

class ListOrderPage extends StatefulWidget {
  ListOrderPage({Key key}) : super(key: key);

  @override
  _ListOrderPageState createState() => _ListOrderPageState();
}

TextStyle titleStyle =
    TextStyle(color: Colors.white, fontFamily: 'open-sans', fontSize: 20);
GlobalKey _keyQuantityOrders = GlobalKey();

class _ListOrderPageState extends State<ListOrderPage> {
  TextStyle style =
      new TextStyle(color: Colors.white, fontFamily: 'open-sans', fontSize: 14);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Лист заказов",
            style: titleStyle,
          ),
          backgroundColor: Color.fromRGBO(255, 130, 67, 1),
        ),
        body: Container(
          child: Column(children: <Widget>[
            Expanded(child: createList()),
          ]),
        ));
  }

  Widget createList() {
    return Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.blue,
        child: FutureBuilder<List<Order>>(
            future: DBProvider.db.getOrders(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Order>> snapshot) {
              if (snapshot.hasData) {
                return Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        Order item = snapshot.data[index];
                        return Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Container(
                                color: Colors.white,
                                width: 175,
                                height: 80,
                                child: Container(
                                    height: 614,
                                    child: InkWell(
                                        child: Card(
                                            child: Container(
                                      color: Colors.blue,
                                      height: 120,
                                      child: Row(
                                        children: [
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(left: 20),
                                              child: Center(
                                                  child: Text(
                                                '${item.id}',
                                                style: style,
                                              ))),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(left: 20),
                                              child: Container(
                                                  width: 100,
                                                  height: 20,
                                                  child: FutureBuilder<User>(
                                                      future: DBProvider.db
                                                          .getUser(item.idUser),
                                                      builder: (BuildContext
                                                              context,
                                                          AsyncSnapshot<User>
                                                              snapshot) {
                                                        if (snapshot.hasData) {
                                                          print(snapshot
                                                              .data.fullname);
                                                          return Text(
                                                            snapshot
                                                                .data.fullname,
                                                            style: style,
                                                          );
                                                        } else
                                                          return Text("Ошибка", style: style,);
                                                      }))),
                                          Padding(
                                              padding:
                                                  EdgeInsets.only(left: 20),
                                              child: Container(
                                                  width: 100,
                                                  height: 20,
                                                  child: Text(
                                                    item.date,
                                                    style: style,
                                                  ))),
                                        ],
                                      ),
                                    ))))));
                      },
                    ));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }

  addOrder() {
    DBProvider.db
        .newOrder(new Order(idUser: 1, date: '2021.03.12', status: 'Принят'));
    DBProvider.db
        .newOrder(new Order(idUser: 2, date: '2021.03.12', status: 'Принят'));
    DBProvider.db
        .newOrder(new Order(idUser: 3, date: '2021.03.12', status: 'Принят'));
    DBProvider.db
        .newOrder(new Order(idUser: 4, date: '2021.03.12', status: 'Принят'));
  }

  addUser(){
    DBProvider.db.newUser(new User(fullname: "Градсков Алексей Владимирович", phone: '89513582788', address: "Львовская, 28", birthday: "15.03.2002", gender: "Мужской"));
  }
}
