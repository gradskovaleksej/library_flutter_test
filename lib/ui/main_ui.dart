import 'dart:collection';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:library_flutter/data/Database.dart';
import 'package:library_flutter/models/Book.dart';
import 'package:library_flutter/models/Cart.dart';
import 'package:library_flutter/models/CartElement.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key, this.context}) : super(key: key);
  BuildContext context;

  @override
  _MainPageState createState() {
    return new _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {
  final cart = Cart();
  TextStyle titleStyle =
      TextStyle(color: Colors.white, fontFamily: 'open-sans', fontSize: 20);
  TextStyle textStyle = TextStyle(fontFamily: 'open-sans', fontSize: 16);

  @override
  Widget build(BuildContext context) {
    addBook();
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Популярное",
            style: titleStyle,
          ),
          backgroundColor: Color.fromRGBO(255, 130, 67, 1),
        ),
        body: SingleChildScrollView(
            child: Column(children: [
          Container(
            height: 355,
            child: Column(children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 20, left: 10, bottom: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Классика",
                        style: TextStyle(color: Colors.black, fontSize: 20)),
                  )),
              Expanded(child: createList(2)),
            ]),
          ),
          Container(
            height: 355,
            child: Column(children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 10, left: 10, bottom: 10),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Наука",
                        style: TextStyle(color: Colors.black, fontSize: 20)),
                  )),
              Expanded(child: createList(4)),
            ]),
          )
        ])));
  }

  //Добавление тестового набора книг
  addBook() {
    DBProvider.db.newBook(new Book(
        idOfGenre: 4,
        isbn: "9785905891960",
        name: "Sapiens. Краткая история человечества",
        author: "Юваль Харари",
        genre: "Наука",
        year: 2017,
        price: 399,
        quantityInStock: 21,
        publisher: "Синдбад",
        quantityOfPages: 570,
        linkImg: "https://fiction-books.ru/img/1026063868.jpg"));
    DBProvider.db.newBook(new Book(
        idOfGenre: 4,
        isbn: "978-5-17-107720-4",
        name: "Краткая история времени",
        author: "Стивен Хокинг",
        genre: "Наука",
        year: 2017,
        price: 578,
        quantityInStock: 15,
        publisher: "АСТ",
        quantityOfPages: 272,
        linkImg:
            "https://cdn1.ozone.ru/s3/multimedia-2/wc1200/6025560650.jpg"));
    DBProvider.db.newBook(new Book(
        idOfGenre: 4,
        isbn: "978-5-17-114385-5",
        name: "Человек, который принял жену за шляпу",
        author: "Оливер Сакс",
        genre: "Наука",
        year: 2019,
        price: 212,
        quantityInStock: 15,
        publisher: "АСТ",
        quantityOfPages: 352,
        linkImg: "https://img-gorod.ru/24/782/2478202_detail.jpg"));
    DBProvider.db.newBook(new Book(
        idOfGenre: 2,
        isbn: "978-5-17-11438534",
        name: "Шинель",
        author: "Н.В. Гоголь",
        genre: "Классика",
        year: 1875,
        price: 243,
        quantityInStock: 15,
        publisher: "Просвещение",
        quantityOfPages: 123,
        linkImg: "https://alifba.ru/storage/img/products/2038/cover1.jpg"));
    DBProvider.db.newBook(new Book(
        idOfGenre: 2,
        isbn: "978-5-17-1143324",
        name: "Тарас Бульба",
        author: "Н.В. Гоголь",
        genre: "Классика",
        year: 1843,
        price: 221,
        quantityInStock: 15,
        publisher: "Просвещение",
        quantityOfPages: 113,
        linkImg: "https://ozon-st.cdn.ngenix.net/multimedia/1020007104.jpg"));
  }

  searchBookInList(List<CartElement> books, Book findBook) {
    int counter = 0;
    for (int i = 0; i < books.length; i++) {
      if (books[i].book.isbn == findBook.isbn) {
        counter++;
      }
    }
    return counter;
  }

  CartElement checkingItemInCart(
      CartElement newElement, List<CartElement> elementsInCart) {
    var element = elementsInCart.firstWhere(
        (cartElement) => cartElement.book.isbn == newElement.book.isbn,
        orElse: () => null);
    return element;
  }

  createElement(List<CartElement> cart, Book newBook) {
    var newElement = new CartElement(
        id: cart.length,
        quantity: searchBookInList(cart, newBook) + 1,
        book: newBook);
    return newElement;
  }

  Widget createList(int idOfGenre) {
    return Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.blue,
        child: FutureBuilder<List<Book>>(
            future: DBProvider.db.getBookForGenre(idOfGenre),
            builder:
                (BuildContext context, AsyncSnapshot<List<Book>> snapshot) {
              if (snapshot.hasData) {
                return Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        Book item = snapshot.data[index];
                        return Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Container(
                                color: Colors.white,
                                width: 175,
                                height: 80,
                                child: Consumer<Cart>(
                                    builder: (context, cart, child) => InkWell(
                                        child: Padding(
                                            padding: EdgeInsets.only(top: 8),
                                            child: Column(children: <Widget>[
                                              item.linkImg == null
                                                  ? Image.network(
                                                      "https://diesel.tigmig.ru/image/cache/no-image-900x.jpg",
                                                      height: 120,
                                                      width: 120,
                                                    )
                                                  : Image.network(
                                                      item.linkImg,
                                                      height: 120,
                                                      width: 120,
                                                    ),
                                              Column(
                                                children: [
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 15, left: 10),
                                                      child: Container(
                                                          height: 39,
                                                          child: Align(
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                item.name,
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                style:
                                                                    textStyle,
                                                              )))),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 10, left: 10),
                                                      child: Container(
                                                          height: 20,
                                                          child: Align(
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              child: Text(
                                                                  "${item.price.toString()} руб.",
                                                                  style:
                                                                      textStyle)))),
                                                ],
                                              ),
                                              Padding(
                                                  padding:
                                                      EdgeInsets.only(top: 20),
                                                  child: Align(
                                                      alignment:
                                                          FractionalOffset
                                                              .bottomCenter,
                                                      // ignore: deprecated_member_use
                                                      child: RaisedButton(
                                                        splashColor:
                                                            Colors.white,
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(0),
                                                        ),
                                                        color: Color.fromRGBO(
                                                            255, 130, 67, 1),
                                                        onPressed: () {
                                                          var element =
                                                              createElement(
                                                                  cart.items,
                                                                  item);
                                                          var resultOfChecking =
                                                              checkingItemInCart(
                                                                  element,
                                                                  cart.items);
                                                          if (resultOfChecking ==
                                                              null) {
                                                            cart.addToCart(
                                                                element);
                                                          } else
                                                            cart
                                                                .items[
                                                                    resultOfChecking
                                                                        .id]
                                                                .quantity += 1;
                                                        },
                                                            child: Text(
                                                                "Добавить в корзину",
                                                                style:
                                                                    textStyle)),
                                                      ))
                                            ]))))));
                      },
                    ));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }
}
