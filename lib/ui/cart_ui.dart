import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:library_flutter/data/Database.dart';
import 'package:library_flutter/models/Cart.dart';
import 'package:library_flutter/models/CartElement.dart';
import 'package:provider/provider.dart';

class CartPage extends StatefulWidget {
  CartPage({Key key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

TextStyle titleStyle =
    TextStyle(color: Colors.white, fontFamily: 'open-sans', fontSize: 20);
GlobalKey _keyTotalPrice = GlobalKey();

class _CartPageState extends State<CartPage> {
  TextStyle style =
      new TextStyle(color: Colors.white, fontFamily: 'open-sans', fontSize: 14);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomBar(
          height: 80,
        ),
        body: Column(children: [
          Consumer<Cart>(
              builder: (context, cart, child) => cart.items.length == 0
                  ? Container(
                      height: 615,
                      child: Center(
                        child: Text(
                            "Ваша корзина пуста. Добавьте товары в главном меню.",
                            style: TextStyle(
                                color: Colors.blue,
                                fontFamily: 'open-sans',
                                fontSize: 18),
                            textAlign: TextAlign.center),
                      ))
                  : _buildItem(context, cart.items)),
          Container(
            color: Colors.blue,
            child: Consumer<Cart>(
                builder: (context, cart, child) => Row(children: [
                      Padding(
                          key: _keyTotalPrice,
                          padding: EdgeInsets.only(left: 30),
                          child: Container(
                            width: 100,
                            child: Text(
                              "Итог: ${cart.total}",
                              style: style,
                            ),
                          )),
                      Padding(
                        padding: EdgeInsets.only(left: 110),
                        child: RaisedButton(
                          onPressed: () {},
                          color: Color.fromRGBO(255, 130, 67, 1),
                          child: Text(
                            "Оформить заказ",
                            style: style,
                          ),
                        ),
                      )
                    ])),
          )
        ]));
  }


  Widget _buildItem(BuildContext context, List<CartElement> cart) {
    return Container(
        height: 614,
        child: ListView.builder(
            itemCount: cart.length,
            itemBuilder: (context, index) {
              var element = cart[index];
              return InkWell(
                  child: Card(
                      child: Container(
                          color: Colors.blue,
                          height: 120,
                          child: Dismissible(
                            background: Container(
                                color: Colors.red,
                                child: Padding(
                                    padding: EdgeInsets.only(left: 20),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        "Удалить",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 16),
                                        textAlign: TextAlign.left,
                                      ),
                                    ))),
                            key: ValueKey(element),
                            onDismissed: (DismissDirection direction) {
                              cart.removeAt(index);
                              setState(() {});
                            },
                            child: Row(
                              children: [
                                Padding(
                                    padding: EdgeInsets.only(left: 20),
                                    child: Center(
                                        child: Text(
                                      (index + 1).toString(),
                                      style: style,
                                    ))),
                                cart[index].book.linkImg == null
                                    ? Image.network(
                                        "https://diesel.tigmig.ru/image/cache/no-image-900x.jpg",
                                        height: 100,
                                        width: 100,
                                      )
                                    : Image.network(
                                        cart[index].book.linkImg,
                                        height: 100,
                                        width: 100,
                                      ),
                                Padding(
                                    padding: EdgeInsets.only(top: 10, left: 10),
                                    child: Column(
                                      children: [
                                        Container(
                                            width: 100,
                                            height: 20,
                                            child: Text(
                                              "${cart[index].book.name}",
                                              style: style,
                                            )),
                                        Container(
                                            width: 100,
                                            child: Padding(
                                                padding:
                                                    EdgeInsets.only(top: 10),
                                                child: Text(
                                                  "${(cart[index].book.author)}",
                                                  style: style,
                                                ))),
                                        Container(
                                            width: 100,
                                            child: Padding(
                                              padding: EdgeInsets.only(top: 10),
                                              child: Text(
                                                "${cart[index].book.year} г.",
                                                style: style,
                                              ),
                                            ))
                                      ],
                                    )),
                                Padding(
                                    padding: EdgeInsets.only(top: 20, left: 20),
                                    child: Column(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              border: Border.all(
                                                  color: Colors.black,
                                                  width: 1)),
                                          width: 30,
                                          child: InkWell(
                                            onTap: () {
                                              cart[index].quantityAdd();
                                              setState(() {});
                                            },
                                            child: Text(
                                              "+",
                                              style: TextStyle(fontSize: 20),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 30,
                                          color: Colors.white,
                                          child: Text(
                                            "${cart[index].quantity}",
                                            style: TextStyle(fontSize: 20),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Container(
                                          width: 30,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              border: Border.all(
                                                  color: Colors.black,
                                                  width: 1)),
                                          child: InkWell(
                                            onTap: () {
                                              cart[index].quantityDecrease();
                                              if (cart[index].quantity < 1)
                                                cart.remove(cart[index]);
                                              setState(() {});
                                            },
                                            child: Text(
                                              "-",
                                              style: TextStyle(fontSize: 20),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        )
                                      ],
                                    )),
                                Padding(
                                    padding: EdgeInsets.only(left: 30),
                                    child: Row(children: [
                                      Container(
                                          child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          maxWidth: 50.0,
                                          maxHeight: 20.0,
                                        ),
                                        child: AutoSizeText(
                                          "${element.totalPrice}",
                                          style: style,
                                          maxLines: 1,
                                        ),
                                      )),
                                      AutoSizeText(
                                        " руб.",
                                        style: style,
                                      )
                                    ]))
                              ],
                            ),
                          ))));
            }));
  }
}

class CustomBar extends PreferredSize {
  final double height;

  CustomBar({this.height = kToolbarHeight});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        decoration: BoxDecoration(color: Color.fromRGBO(255, 130, 67, 1)),
        child: Consumer<Cart>(
            builder: (context, cart, child) =>
                Row(
                  children: [
                    Padding(
                        padding: EdgeInsets.only(top: 20, left: 20),
                        child: Text(
                          "Корзина",
                          style: titleStyle,
                        )),
                    Padding(padding: EdgeInsets.only(left: 200)),
                    Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: InkWell(
                          onTap: () {
                            cart.clearCart();
                          },
                          child: Text(
                            "Очистить",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontFamily: 'open-sans'),
                          ),
                        ))
                  ],
                )));
  }
}
