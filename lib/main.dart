
import 'package:flutter/material.dart';
import 'package:library_flutter/admin_ui/orders_ui.dart';
import 'package:library_flutter/models/OrderList.dart';

import 'package:library_flutter/ui/cart_ui.dart';
import 'package:library_flutter/ui/main_ui.dart';
import 'package:provider/provider.dart';

import 'models/Cart.dart';

void main() {
  runApp(
    ChangeNotifierProvider(create: (context) => OrderList(), child: MyApp())
  );
}

class MyApp extends StatelessWidget {
  final Cart model;
  final OrderList model1;

  const MyApp({Key key, @required this.model, @required this.model1}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  List<Widget> widgets = <Widget>[ListOrderPage(), CartPage()];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: widgets.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Главная',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'Корзина',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue,
        onTap: _onItemTapped,
      ),
    );
  }
}
